# Version Control With Git

## Overview On Version Control
In software engineering, version control (also known as revision control, source code management and some other terms) is in specific the process of managing changes to source code, documents, et cetera as well as – to a limited extent – binary data.<br/>
More generally however, version control nowadays includes much more like:
* managing the code itself (searching through it, “blaming”, bisecting, et cetera)
* assisting in collaborative working
* providing support for external systems like issue tracking, continuous integration and much more
* release management
* many “little things”, like automatic changelog generation

and is itself integrated into:
* external systems as mentioned above
* Integrated Development Environments (like Eclipse)
* Web services like GitLab or GitHub
* and much more


Two central aspects of version control are **commits** and **revisions**:<br/>
In version control, one doesn’t record every single change (like each and every added or removed character as the editor types) to the data, but only sets of changes that belong somehow together. Such a set of changes – when stored – is called a **commit**, the process of storing it **committing**.<br/>
Each commit leads to a so called **revision**, which is the state of *all* the managed data at a certain point in its history. All revisions have unique identifiers.<br/>
Sometimes the terms “commit” and “revision” are used interchangeably.

All data (that is: all commits/revisions) and more is stored in the so called **repository**, which is more or less a database of the actual data’s history.

A version control system (VCS) is the software that implements version control. Amongst others, it provides an interface to and manages the repository.


Typically one differentiates between two kinds of version control:
* centralised<br/>
  There is one central repository (typically on some server), while users have only parts of that repository or even just a certain revision (so called “working copy”).<br/>
  One big disadvantage is obviously that the central repository needs to accessible for many version control operations. Some of these kind also use a lock-modify-write schema, in which a file can be edited by only one user at the same time.
* distributed<br/>
  Every user has his own full copy of the repository (including all the history) and commits are “exchanged” between them.<br/>
  Typically, users agree upon one central repository (or in very big projects even a tree of central repositories), which are used as synchronisation and exchange points. These are however (mostly) in no way special, apart from the convention as being used as central repositories.<br/>
  The advantage is obviously that access to the other repositories is only needed when data is to be synchronised/exchanged, but not for any other version control operations. Further and conceptually, all of this kind use a copy-modify-merge schema in which all users can edit a file at the same time (but may need to resolve merge conflicts). A disadvantage is that the full history of very large projects can consume considerable amounts of storage (for example, the whole Linux kernel repository requires ~3,5 GB; as of 2020-08 and depending on compression).


Some notable examples of version control systems:
* centralised (*mostly historical*)<br/>
  Revision Control System (RCS), Concurrent Versions System (CVS), Subversion (SVN)
* distributed<br/>
  git, Mercurial, arch, Bazaar




## Overview On Git
The by far most powerful and widely used VCS is git, on which this document focuses.

It was originally created in April 2005 by Linus Torvalds for the Linux kernel and is nowadays maintained by Junio Hamano and many more.<br/>
Some of its characteristics and paradigms include:
* free and open source
* distributed
* very powerful branching and merging
* fast and highly scalable
* integrated cryptography for authentication and integrity protection
* supports a wide range of usage scenarios: from simple to extremely powerful and complex processes
* re-uses existing standards and protocols (like HTTP and SSH for communication)
* consists of many little but very powerful tools and wrappers around them (UNIX philosophy: “small is nice”), providing both a high-level view but, if necessary, also access to the deep internals
* large number of 3rd-party “extensions” and programs around git, both for terminal as well as graphical
* interfaces to many other version control systems
* very active development community and supported/used by very big companies
* extensive documentation in form of manpages, tutorials and videos




## Git Basics
### Commands
When working on the terminal with [git](https://git-scm.com/docs/git) itself (in especially: when not using some kind of front-end) every interaction happens via a command in the form of:
```shell
$ git [<options>…] <command-name> [<arguments>…]
```
`<options>` are global options applicable to all commands, which are however rarely needed.<br/>
`<command-name>` specifies the command to be executed, for example `commit`, `log` or `status`.<br/>
`<arguments>` are either options (like `--path` or `--author=<name>`) or non-option-arguments to the respective command, which means that every command has different options and even if some are named equal, their effect depends on the respective command.

Commands that operate on a repository search from the current working directory up to the root `/` for a `.git` directory in order to determine the repository to be used.


### Help / Manpages
Git comes with extensive [documentation](https://git-scm.com/doc).

[Each command](https://git-scm.com/docs/git#_git_commands) is described in detail by its corresponding [manual page](https://git-scm.com/docs):
```shell
$ man git-<command-name>
```
Further manpages of interest include [gitrevisions](https://git-scm.com/docs/gitrevisions), [gitglossary](https://git-scm.com/docs/gitglossary), [gitworkflows](https://git-scm.com/docs/gitworkflows), [giteveryday](https://git-scm.com/docs/giteveryday), [gitfaq](https://git-scm.com/docs/gitfaq) as well as [gittutorial](https://git-scm.com/docs/gittutorial) and [gittutorial-2](https://git-scm.com/docs/gittutorial-2).

In addition there is the book “[Pro Git](https://git-scm.com/book/)” from 2014, which is freely available in several languages, and countless of further tutorials and videos.


### Configuration
The [`git config`](https://git-scm.com/docs/git-config) command gets and sets configuration options on several levels:
* `--system`<br/>
  System-wide for all users (requires privileges).
* `--global`<br/>
  Globally (that is for all repositories) per user.
* `--local`<br/>
  Per (clone of a) repository.
* `--worktree` and `--file`<br/>
  *see documentation*

Some options that one initially might want to set include:
```shell
$ git config --global user.name  'John Doe'
$ git config --global user.email 'john.doe@example.org'
$ git config --global init.defaultBranch 'master'
```


### Initialising And Cloning Repositories
For most real world cases one either *initialises* (“creates”) an empty repository (typically when starting some new project from scratch) or *clones* an existing repository (typically when one wants to contribute to a project or read its data).

Initialising:
```shell
$ git init someRepostitoryName
```
will initialise an empty repository in the directory `someRepostitoryName` within the current working directory.

Cloning:
```shell
$ git clone ssh://git@gitlab.physik.lmu.de/mitterer/git-course-example.git
```
*or similar forms like:*
```shell
$ git clone https://gitlab.physik.lmu.de/mitterer/git-course-example.git
```
will clone an existing repository to the directory `git-course-example` within the current working directory.

The process of cloning retrieves (nearly) **all data** contained within the specified remote repository, in especially the whole history of commits (of all branches).<br/>
This is in contrast to the aforementioned centralised VCS.

It shall be noticed, that git web-services like GitLab and GitHub use the term *forking* in a slightly different way:<br/>
Usually in that context, forking is the process of “splitting” development of some open source project (sometimes for political or social reasons). For example, LibreOffice is a fork of OpenOffice (with both projects continuing to exist).<br/>
In such git web-services forking is usually the process of cloning an existing repository into one’s own domain **on the provider’s servers**, for example the above `ssh://git@gitlab.physik.lmu.de/mitterer/git-course-example.git` to `ssh://git@gitlab.physik.lmu.de/GDuckeck/git-course-example.git`. This can be meant as a fork in the usual way (as described above) but often it’s just serves as a “service repository”, used to create pull requests against the original repository.


### Remotes
A *remote* is another repository one wants to “connect” (in the sense of pulling/pushing data from/to) to a repository.

This can be used in many different ways:
* an official and/or central repository, used by developers to coordinate/exchange their commits
* another developer’s repository, with which commits can be directly exchanged
* a “service repository” for git web-services, located on the provider’s servers *(see above at “Initialising And Cloning Repositories”)*

and more advanced concepts like:
* tracking the repositories of 3rd-party or sub-projects within a (“main”) repository (so called “subtree merging”)


Remotes are identified by a name, located via an URL and managed with [`git remote`](https://git-scm.com/docs/git-remote).

The name is used in many git commands, for example:
```shell
$ git pull upstream
$ git push origin
```

By convention, `upstream` is used for the remote of the canonical repository of a project, if any. Typically, this will be the “official” and/or “central” repository, where all developers coordinate/exchange their commits.

By convention, `origin` is used for the remote from which the repository originates, if any.<br/>
This can be the “service repository” used with git web-services *(see above at “Initialising And Cloning Repositories”)* but it might also be identical to `upstream`. A cloned repository will by default have its `origin` set to the remote from where it was cloned.


Several schemas can be used as URL, including the above mentioned ones for git over SSH and HTTPS. See [git-fetch, section “GIT-URLS”](https://git-scm.com/docs/git-fetch#URLS) for a detailed description.

*Note that SSH can be generally considered to be more secure than HTTPS, but this strictly requires secure verification (that is: via some trusted path) of the remote’s SSH public key.<br/>
For `gitlab.physik.lmu.de` the SSH public key fingerprints are:*
```
 521 SHA256:kd6U/XPLB2OxpVgPqaKK15trEfwrLDbjbqpo9FAhux8 gitlab.physik.lmu.de (ECDSA)
2048 SHA256:UEqvcxgAOygDU+mSmwV/ZSXHkIhgWd0gE9syUYrlj5I gitlab.physik.lmu.de (RSA)
```
*That is, if one trusts the author of this document!<br/>
If a secure verification is not possible HTTPS might be an alternative.*


### (Internal) Repository Layout
The repository layout defines the internal data structures of a git repository and is described in detail in [gitrepository-layout](https://git-scm.com/docs/gitrepository-layout).<br/>
For the purpose of this document, the following extremely brief overview shall be enough:

The actual repository is **fully** contained in a directory named `.git` **at the root** of the working area (that is: the directory created by `git init` or `git clone`).

Except for the working area (these are the current files from the repository and others, upon which one works) that directory contains literally everything: the whole history of commits and all meta-data (like branches, remotes or repository-local configuration).<br/>
Without expert knowledge one should typically not touch any files within it.

For an empty repository this looks like:
```
emptyRepostitory/
└── .git/
    ├── branches/
    ├── config
    ├── description
    ├── HEAD
    ├── hooks/
    │   ├── applypatch-msg.sample
    │   ├── commit-msg.sample
    │   ├── fsmonitor-watchman.sample
    │   ├── post-update.sample
    │   ├── pre-applypatch.sample
    │   ├── pre-commit.sample
    │   ├── pre-merge-commit.sample
    │   ├── prepare-commit-msg.sample
    │   ├── pre-push.sample
    │   ├── pre-rebase.sample
    │   ├── pre-receive.sample
    │   └── update.sample
    ├── info/
    │   └── exclude
    ├── objects/
    │   ├── info/
    │   └── pack/
    └── refs/
        ├── heads/
        └── tags/
```

For a non-empty repository this looks like:
```
nonEmptyRepostitory
├── .git/
│   ├── branches/
│   ├── COMMIT_EDITMSG
│   ├── config
│   ├── description
│   ├── HEAD
│   ├── hooks/
│   │   …
│   ├── index
│   ├── info/
│   │   ├── exclude
│   │   └── refs
│   ├── logs/
│   │   ├── HEAD
│   │   └── refs/
│   │       ├── heads/
│   │       │   ├── devel
│   │       │   ├── master
│   │       │   └── minor-cosmetic-improvements
│   │       └── remotes/
│   │           ├── origin/
│   │           │   ├── devel
│   │           │   └── master
│   │           └── upstream/
│   │               └── master
│   ├── objects/
│   │   ├── 10/
│   │   │   └── 21df29e5d4b5752146cdac752f1f717e1fa20f
│   │   ├── 46/
│   │   │   ├── 19bd90c9866f2eb6312a9c97d8b52756765206
│   │   │   └── aa9b4edd903068d3eda37507fe36029aaf7cbd
│   │   ├── 5e/
│   │   │   └── b8153df37ba1a0eb6e71e04043648cda36cb77
│   │   ├── f6/
│   │   │   ├── 0b17c8a52257f7b62e9e9daa8262831052dc1f
│   │   │   ├── 4488ac36440cb32e7223a923ffc94101c4f6e5
│   │   │   └── 569d1f89750ab036ef00b11d0a17b0a5c7cc70
│   │   ├── info/
│   │   │   ├── commit-graph
│   │   │   └── packs
│   │   └── pack/
│   │       ├── pack-37b5e0a302c17c3af2186fc3397037739780f1b1.idx
│   │       └── pack-37b5e0a302c17c3af2186fc3397037739780f1b1.pack
│   ├── packed-refs
│   └── refs/
│       ├── heads/
│       │   ├── devel
│       │   ├── master
│       │   └── minor-cosmetic-improvements
│       ├── remotes/
│       │   ├── origin/
│       │   └── upstream/
│       └── tags/
│           ├── v1.0.0
│           └── v2.0.0
│
│
├── LICENCE
├── README
├── doc
│   …
└── src
    …
```
The most interesting here are probably the objects which contain the actual data in either plain or packed (that is: delta-compressed) form.


### Repository Logical Structure, Commit Hierarchy/Tree And Branches
The logical structure of a repository, which is the hierarchy of all commits, is a tree (or even several), as visualised by the following example which shows all (that is: from all branches and even from different roots) commits in chronological order (from bottom to top):

![visualisation of a commit tree](https://gitlab.physik.lmu.de/mitterer/git-course/-/raw/master/res/commit-tree-example.svg)

A non-empty repository contains at least one **root, which is an orphaned commit**.

**Any other commit has at least one parent**, which it is based upon.<br/>
For example in programming, as development moves on, every single commit *(which are sets of changes that belong somehow together)* adds some new functionality to, fixes issues from, et cetera the previous state of the code, which just happens to be in the parent revision/commit.


A very powerful feature are **branches** and the process of **merging** them.

Branches can be created at/from any commit, which will then be the parent respectively ancestor of all commits in the newly created branch. It is possible to create branches from branches.<br/>
By convention, the main branch (which is typically used as the canonical one, where all “final” commits go) is named `master`. But how this is actually used, depends upon the respective project. For example in programming, some may use `master` as the branch which tracks the current development, having additional “release branches”, which just track bugfixes to supported stable versions of the program.<br/>

Creating a branch is happening instantaneously (in git). One can switch back and forth between all branches, with anything that is committed (into any branch) not being lost. When a switch (in git called *checkout*) takes place, the working area is brought to the status of the respective branch (more precisely: its tip), which is again very fast.<br/>
As visualised above, commits can be made interchangeably to any branch, which effectively means that development can move on concurrently in any branches. This make branching crucial to collaborative development.

There are **many** uses cases of branches, amongst others:
* As a “working zone” for some (typically larger or longer) development (for example a new feature or a more invasive bugfix), which – while being intended to be merged into – shall meanwhile not interfere with the development in `master` (nor vice-versa). If the work turns out be promising it can be eventually merged, if not, the branch can be simply deleted.
* As a “separate line” of development, which is not intended to be merged back (typically into `master`).<br/>
  For example, the current development could take place in `master`, while stable releases (like version 1.x.x or 2.x.x) are maintained in their own branches and receive any development (like bugfixes) there.
* To indicate in the tree, that commits belong together and shall only be merged as a set and not independently.
* For organisational or administrative reasons.<br/>
  One example could be that `master` shall be only writeable by certain very experienced and/or trustworthy developers while all other contributors need to put their commits into other branches (from which the “main” developers might pick them up).
* Similarly to that, git web-frontends organise pull requests (from the contributor’s “fork” of a repository (“`origin`”) into the “main” developer’s repository (“`upstream`”)) usually via branches.

The “opposite” of branching is merging, which is “integrating” the commits of some branch(es) into another. While merging can happen back and forth between branches, it’s usually done in only one direction.

Git has very advanced merging strategies, but obviously, if any two commits (which are not ancestor respectively descendant of each other) modify the same area of a file a so called *merge conflict* might arise.<br/>
This conflict needs to be resolved by the merging user (who can use several tools which facilitates this, for example [`git mergetool`](https://git-scm.com/docs/git-mergetool)).

*There are several more very powerful techniques similar to merging (in that they apply commits of some branch to another), most notably rebasing and cherry-picking, which are however beyond this document’s scope.*


### Revision/Commit IDs
Each revision respectively commit (within each branch and even “dangling” commits) has a “unique” ID (internally this is actually an object ID).

Simplified, that ID is calculated as a hash value over the actual data (that is: the changed files) as well as meta-data like the commit message, author, times and the parent(s) of the commit.<br/>
As of now, SHA-1 is used per default as the cryptographic hash algorithm, but due to its weaknesses a migration to SHA-2-256 is on the way.<br/>
*It shall be noted, that it is this tree (or chain) of hashes (a so called “Merkle tree”), which gives full integrity protection of the whole repository history.*

For SHA-1 a full commit ID looks like:
```
dae86e1950b1277e545cee180551750029cfe735
```


### Revision Specifiers
Throughout git and its commands, revision specifiers are used to specify single revisions or ranges of revisions.

There are plenty of schemas for revision specifiers, all of which are described in [gitrevisions](https://git-scm.com/docs/gitrevisions).

Some for single revisions are:
* *revision/commit ID*<br/>
  The full SHA-1 (or SHA-2-256) ID of the revision/commit (for example `dae86e1950b1277e545cee180551750029cfe735`), which is often shortened to the first 6 hexadecimal digits (for example `dae86e`), but in principle any prefix string can be used, as long as there is no ambiguity.
* *symbolic revision name*<br/>
  The names of branches (for example `master`) or tags (may for example be used for versions like `1.0.0`) or the special name `HEAD`, which is the commit on which the working area of the repository is based.
* `<rev>~[<n>]`<br/>
  Referring to the `<n>`th parent of the commit `<rev>`.
* `[<refname>]@{<date>}`<br/>
  Symbolic revision names with a time constraint (for example `master@{yesterday}`, for the branch `master` a day ago).


Some for revision ranges are:
* `<rev1>`<br/>
  Any commits reachable form `<rev1>` (that is: `<rev1>` and all it’s ancestors).
* `^<rev1>`<br/>
  Any commits except those reachable form `<rev1>` (that is: anything excluding `<rev1>` and all it’s ancestors).
* `<rev1>..<rev2>`<br/>
  Can be thought of as “any commit from excluding `<rev1>` up to including `<rev2>`” but is more exactly any commits reachable from `<rev2>` excluding those reachable from `<rev1>`.




## Simple Git Workflow
The following examples demonstrate a simple (that is: single developer) workflow in git, assuming a newly started programming project.<br/>
Alongside some basic git commands for “daily work” are introduced.


### Preparations
1. First some basic configuration shall be set:
   1. Configuring the user’s name and e-mail address:
      ```shell
      $ git config --global user.name  'Christoph Anton Mitterer'
      $ git config --global user.email 'christoph.anton.mitterer@lmu.de'
      $ git config --global init.defaultBranch 'master'
      ```
      These will be used for the author/committer meta-data of commits.
   2. Optionally, configure one’s favourite editor as well as diff- and merge-tools:
      ```shell
      $ git config --global core.editor vim
      $ git config --global diff.tool meld
      $ git config --global diff.guitool meld
      $ git config --global merge.tool meld
      $ git config --global merge.guitool meld
      ```
   3. Optionally, define some command aliases that might be handy:
      ```shell
      $ git config --global alias.s status
      $ git config --global alias.dt 'difftool --dir-diff'
      ```
      Will make `git s` and `git dt` aliases to `git status` and `difftool --dir-diff`, respectively.
2. Next, a new repository shall be initialised:
   ```shell
   $ git init git-course-example
   ```
   ```
   Initialized empty Git repository in …/git-course-example/.git/
   ```

For any following git command to operate on that repository, the current working directory needs to be it or any subdirectory thereof, therefore:

3. Change the current working directory to the repository’s working area:
   ```shell
   $ cd git-course-example
   ```


### Inspecting The Repository Status
One of the most important commands is [`git status`](https://git-scm.com/docs/git-status), which shows the current status of the repository.

For an empty repository this looks like:
```shell
$ git status
```
```
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```
telling, that the current branch is `master`, that there are no commits, yet, and that there is nothing which would get committed (if one would try to).


### Tracked And Untracked Files
Inside the working area of a repository, files are either *untracked* or *tracked* by git. Files in that sense are regular files or symbolic links – directories aren’t tracked at all.

Tracking means that git “looks after” or considers files in certain circumstances, for example it will tell if a tracked file has been modified since it was *staged* (see below) or committed.

For these examples a file `prime-number-test.c++` shall be created with the following content:
```c++
#include <iostream>


int main(int argc, char* argv[])
{
	int number = 73; //why 73?
	
	
	//check whether number is a prime
	bool isPrime = true; //assume the number was a prime
	
	for(int i = 1; i <= number; ++i)
	{
		if(number % i  ==  0)
		{
			//found a divisor, thus number cannot be prime
			isPrime = false;
		}
	}
	
	//print results
	if(isPrime)
	{
		std::cout << number << " is a prime number." << std::endl;
	}
	else
	{
		std::cout << number << " is not a prime number." << std::endl;
	}
}
```
*This program in the C++ programming language is intended to check whether a number is a prime.*

Once created, the status is as follows:
```shell
$ git status
```
```
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	prime-number-test.c++

nothing added to commit but untracked files present (use "git add" to track)
```
What has changed is that git now recognises the newly created file as untracked. Still, nothing would get committed, because nothing has been staged.

To make a file tracked some of its content must either be staged or have been committed before.


### Staging Changes
Any changes that shall be committed must first be **staged**, which can be thought of as “marking” them to be committed. In contrast to that are **unstaged** changes, which are those that would not get committed.

New files or modifications to already tracked ones are staged with [`git add`](https://git-scm.com/docs/git-add).<br/>
Files that have already been committed and shall subsequentially be moved/renamed or removed must be staged for that with [`git mv`](https://git-scm.com/docs/git-mv) respectively [`git rm`](https://git-scm.com/docs/git-rm).

Again, only (content from) regular files or symbolic links can be staged – directories are automatically included on demand (which also means that one cannot stage or commit empty directories).

It shall be emphasised that git actually tracks content – not files. What may seem like nitpicking is important because it has several implications:
* If a modified file (respectively content) that has already been staged is modified again, than these modifications need to be staged again, if they shall get committed – otherwise only the previous modifications would.
* It’s possible to stage just parts of a file (down to single characters).<br/>
  This can be done for example with the `--patch` respectively `-p` options to `git add` or 3rd-party add-on commands like [`git crecord`](https://github.com/andrewshadura/git-crecord).
* Consequently, a single file can have both, staged **and** unstaged changes.<br/>
  *This comes in handy if one wants to split several modifications into different commits.*

1. Add the new file (actually: all its content) to be staged:
   ```shell
   $ git add prime-number-test.c++
   ```
2. Inspecting the repository status:
   ```shell
   $ git status
   ```
   ```
   On branch master
   
   No commits yet
   
   Changes to be committed:
     (use "git rm --cached <file>..." to unstage)
   	new file:   prime-number-test.c++
   
   ```
   tells that a new file `prime-number-test.c++` would get committed.


### Committing
The staged changes (that is: any added content as well as moved/renamed or removed files) are what makes up the commit when committing, which in turn leads to a new revision.<br/>
Next to the actual changes, git also stores meta-data like the commit message, author, times and the parent(s) of the commit.

Writing **“good” commit messages is of great importance (they are one of the building blocks of version control)** and lays in the responsibility of the committer.

By convention a commit message starts with a summary line, which roughly describes what the commit does (like “improved primality test by using the sieve of Eratosthenes” or “fixed division by zero bug”), which is optionally followed by a blank line and more detailed information about what was changed any why.<br/>
This can also include rationale and motivation, test results or references like the ID of a bug which was fixed by the commit.

Committing is performed by [`git commit`](https://git-scm.com/docs/git-commit), which allows to specify a commit message either via the `--message=<msg>` respectively `-m <msg>` options (with `<msg>` being the message) or – if not given – opens an editor for that.

1. Commit the previously staged content:
   ```shell
   $ git commit
   ```
   This opens an editor with an empty commit message and some helpful comments:
   ```
   
   # Please enter the commit message for your changes. Lines starting
   # with '#' will be ignored, and an empty message aborts the commit.
   #
   # On branch master
   #
   # Initial commit
   #
   # Changes to be committed:
   #       new file:   prime-number-test.c++
   #
   ```
   A “good” commit message could be:
   ```
   imported prime-number-test.c++ program
   
   This program checks whether a given number is prime and prints the result to
   standard output.
   ```
   After saving and exiting the editor `git commit` will print out some information on what it has done:
   ```
   [master (root-commit) 6d52d36] imported prime-number-test.c++ program
    1 file changed, 30 insertions(+)
    create mode 100644 prime-number-test.c++
   ```
   Here `6d52d36` is the “shortened” commit ID, which – as mentioned earlier – depends also on “variable” things like author and times.

At this point, the committed changes are stored “permanently” in the repository. While there are operations to delete parts of the repository (like branches) or even rewrite the history, commits will normally not get “lost”.

2. Inspecting the repository status:
   ```shell
   $ git status
   ```
   ```
   On branch master
   nothing to commit, working tree clean
   ```
   tells that nothing is staged to be committed **and** nothing has changed (in the working area) since the most recent commit (of the current branch).


### Working In The Working Area
As the name indicates, one actually “works” in the repository’s working area, which isn’t just managing the repository itself or editing its files but also things like compiling, debugging and so on.

1. Compile and execute the program:
   ```shell
   $ g++ prime-number-test.c++
   $ ./a.out
   ```
   ```
   73 is not a prime number.
   ```
   *Something seems wrong here!*

2. Using [gitignore](https://git-scm.com/docs/gitignore) to ignore untracked files:<br/>
   As the repository status shows:
   ```shell
   $ git status
   ```
   ```
   On branch master
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
   	a.out
   
   nothing added to commit but untracked files present (use "git add" to track)
   ```
   there’s now a new untracked file `a.out`, which happens to be the binary created during the compilation above.
   
   Such “generated” files or similar things like backup files should usually not get committed to the repository. It’s however also annoying to have a possibly large list of files shown as untracked.
   
   The solution is to mark them as **ignored**, which can be done by adding filenames or patterns (one per line) into one of `$XDG_CONFIG_HOME/git/ignore`, `.git/info/exclude` of the repository or a `.gitignore` in any directory of the working area.<br/>
   Which of them is used actually makes a difference and is described in the [documentation](https://git-scm.com/docs/gitignore).
   
   Here `.gitignore` shall be used and created with the following content:
   ```gitignore
   a.out
   ```
   
   The repository status now shows:
   ```shell
   $ git status
   ```
   ```
   On branch master
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
   	.gitignore
   
   nothing added to commit but untracked files present (use "git add" to track)
   ```
   `.gitignore` itself as an untracked file.
   
   Some might “solve” this by committing it to the repository (thereby making it tracked), but one should consider that this will affect any other people working on the project.<br/>
   A more conservative approach would be to simply ignore `.gitignore` itself, here for example by setting the its content to:
   ```gitignore
   .gitignore
   a.out
   ```


### More Changes And Inspecting Differences
Above it was found that the example program is faulty, because it claims 73 not to be prime – while in fact it is *(and so much more)*. Moreover, a little improvement and licence information shall be added.

1. The error is in line number 12 of `prime-number-test.c++` which shall be modified to:
   ```c++
   	for(int i = 2; i < number; ++i)
   ```
2. Once a factor has been found it makes no sense to continue testing for any further, so the `for`-loop can be exited be inserting the following after line number 17 of `prime-number-test.c++`:
   ```c++
   			break;
   ```
3. Create a file `LICENCE` with the following content:
   ```
   This program is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 3 of the License, or (at your option) any later
   version.
   This program is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
   PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   You should have received a copy of the GNU General Public License along with
   this program. If not, see <http://www.gnu.org/licenses/>.
   ```
4. Inspecting the repository status:
   ```shell
   $ git status
   ```
   ```
   On branch master
   Changes not staged for commit:
     (use "git add <file>..." to update what will be committed)
     (use "git restore <file>..." to discard changes in working directory)
   	modified:   prime-number-test.c++
   
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
   	LICENCE
   
   no changes added to commit (use "git add" and/or "git commit -a")
   ```
   shows that `prime-number-test.c++` has been modified and also the newly created but untracked `LICENCE`.<br/>
   
   These are however not yet staged (not even the modifications to the already tracked file), so neither of them would get committed.<br/>
   This behaviour is different from some other VCS, in which changes to tracked files would be “automatically” considered to be staged – a behaviour which can be mimicked using `git commit`’s options `--all` respectively `-a`, which automatically stage any tracked files that have been modified or deleted.

Inspecting the actual differences is very important for development work and can be done with, amongst others, the commands [`git diff`](https://git-scm.com/docs/git-diff) and [`git difftool`](https://git-scm.com/docs/git-difftool) as well as many further 3rd-party programs like [icdiff](https://github.com/jeffkaufman/icdiff), [Meld](https://meldmerge.org/) and [KDiff3](https://kde.org/applications/development/kdiff3).<br/>
They produce output either in a number of *diff formats* or as some graphical visualisation.

5. Showing unstaged differences to the “most recent” commit (“`HEAD`”)…<br/>
   …for all files:
   ```shell
   $ git diff
   ```
   …or only for some files:
   ```shell
   $ git diff prime-number-test.c++
   ```
   ```diff
   diff --git a/prime-number-test.c++ b/prime-number-test.c++
   index 99457b9..1ce986c 100644
   --- a/prime-number-test.c++
   +++ b/prime-number-test.c++
   @@ -9,12 +9,13 @@ int main(int argc, char* argv[])
    	//check whether number is a prime
    	bool isPrime = true; //assume the number was a prime
    	
   -	for(int i = 1; i <= number; ++i)
   +	for(int i = 2; i < number; ++i)
    	{
    		if(number % i  ==  0)
    		{
    			//found a divisor, thus number cannot be prime
    			isPrime = false;
   +			break;
    		}
    	}
    	
   ```
   *(Why is the output of both the same here?)*

That so called **unified diff format** is built up as follows:<br/>
After some header lines follow one or more **hunks**, each introduced with some range information starting with `@@`.<br/>
For each hunk, lines are prefixed with either a single `-` (for removed lines), a single `+` (for added lines) or a single ` ` (for unmodified *contextual* lines).

6. Staging and committing the changes:<br/>
   The above changes (from steps (1) to (3)) are rather unrelated to each other. Therefore it would be usually bad style to put them all into one commit – instead three shall be made:
   1. Use `git add`’s option `--patch` respectively `-p` to selectively stage the modifications from (1) and commit them afterwards:
      ```shell
      $ git add --patch prime-number-test.c++
      ```
      ```diff
      diff --git a/prime-number-test.c++ b/prime-number-test.c++
      index 99457b9..1ce986c 100644
      --- a/prime-number-test.c++
      +++ b/prime-number-test.c++
      @@ -9,12 +9,13 @@ int main(int argc, char* argv[])
       	//check whether number is a prime
       	bool isPrime = true; //assume the number was a prime
       	
      -	for(int i = 1; i <= number; ++i)
      +	for(int i = 2; i < number; ++i)
       	{
       		if(number % i  ==  0)
       		{
       			//found a divisor, thus number cannot be prime
       			isPrime = false;
      +			break;
       		}
       	}
       	
      (1/1) Stage this hunk [y,n,q,a,d,s,e,?]? 
      ```
      The presented hunk is however too coarse-grained, so it must be *split* into smaller ones by using `s`:
      ```diff
      Split into 2 hunks.
      @@ -9,9 +9,9 @@
       	//check whether number is a prime
       	bool isPrime = true; //assume the number was a prime
       	
      -	for(int i = 1; i <= number; ++i)
      +	for(int i = 2; i < number; ++i)
       	{
       		if(number % i  ==  0)
       		{
       			//found a divisor, thus number cannot be prime
       			isPrime = false;
      (1/2) Stage this hunk [y,n,q,a,d,j,J,g,/,e,?]? 
      ```
      The first hunk shall be staged by using `y`.
      ```diff
      @@ -13,8 +13,9 @@
       	{
       		if(number % i  ==  0)
       		{
       			//found a divisor, thus number cannot be prime
       			isPrime = false;
      +			break;
       		}
       	}
       	
      (2/2) Stage this hunk [y,n,q,a,d,K,g,/,e,?]? 
      ```
      The second one shall remain unstaged by using `n`.
      
      A `git diff` would now only show the differences from (2), because those from (1) are already staged and those from (3) are not even tracked. To show the staged differences the options `--cached` respectively `--staged` may be used.<br/>
      A `git status` would now report `prime-number-test.c++` as both modified and staged **as well as** modified and unstaged.
      
      Commit the staged changes via:
      ```shell
      $ git commit
      ```
      with a commit message like:
      ```
      fixed a bug during the check for factors
      
      Obviously the number 1 and the prime number candidate itself are always factors
      and must not be checked for, since otherwise any number will be considered as
      prime.
      ```
   2. Use `git add` to stage the remaining modifications from (2) and commit them afterwards:
      ```shell
      $ git add prime-number-test.c++
      $ git commit
      ```
      with a commit message like:
      ```
      improved the check for factors
      
      Once a factor has been found the number is definitely not prime and thus the
      check for any further factors can be aborted.
      ```
   3. Use `git add` to stage the newly created file from (3) and commit it afterwards:
      ```shell
      $ git add LICENCE
      $ git commit -m 'imported licence information'
      ```

`git diff` is much more powerful than what was shown above. For example, it has several forms of invocation, allowing to specify between which commits it compares. A detailed description can be found in its [documentation](https://git-scm.com/docs/git-difftool).

7. For example, showing the differences between two commits:<br/>
   ```shell
   $ git diff HEAD~3..HEAD~1
   ```
   gives the differences from (excluding) 3 commits before `HEAD` to (including) 1 commit before `HEAD`, with `HEAD` being the commit on which the working area is currently based upon (which is the “most recent” commit of the current branch).<br/>
   Right now this is also the *“same”* as:
   ```shell
   $ git diff 6d52d36..68ff634
   ```
   In other words: the changes of (1) and (2) from above.
   ```diff
   diff --git a/prime-number-test.c++ b/prime-number-test.c++
   index 99457b9..1ce986c 100644
   --- a/prime-number-test.c++
   +++ b/prime-number-test.c++
   @@ -9,12 +9,13 @@ int main(int argc, char* argv[])
    	//check whether number is a prime
    	bool isPrime = true; //assume the number was a prime
    	
   -	for(int i = 1; i <= number; ++i)
   +	for(int i = 2; i < number; ++i)
    	{
    		if(number % i  ==  0)
    		{
    			//found a divisor, thus number cannot be prime
    			isPrime = false;
   +			break;
    		}
    	}
    	
   ```
   *(What happens for `HEAD~1..HEAD~3` respectively `68ff634..6d52d36`?)*

It’s often very handy to get such a *diff* for a whole directory tree, which can be done using `git difftool`’s options `--dir-diff` respectively `-d`.<br/>
To get output visualised in a graphical diff tool, the options `--gui` respectively `-g` may be added.


### Inspecting The Commit Tree And The Log
There are many ways to inspect the commit tree and the log, including [`git log`](https://git-scm.com/docs/git-log) as well as 3rd-party programs like [tig](https://jonas.github.io/tig/) and [gitg](https://wiki.gnome.org/Apps/Gitg/).

1. Printing the log for all commits leading to `HEAD` (which is the “most recent” commit of the current branch):
   ```shell
   $ git log
   ```
   ```
   commit 83b93af8d488844903245b3f475384f8c99faa4f (HEAD -> master)
   Author: Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
   Date:   Wed Sep 23 14:31:22 2020 +0200
   
       imported licence information
   
   commit 68ff634b3ccf199986e1ccc6b14382de6a0d52f1
   Author: Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
   Date:   Wed Sep 23 14:30:08 2020 +0200
   
       improved the check for factors
       
       Once a factor has been found the number is definitely not prime and thus the
       check for any further factors can be aborted.
   
   commit 3b34a1f452fee25a83a740d9bf301a8f492782ee
   Author: Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
   Date:   Wed Sep 23 14:27:55 2020 +0200
   
       fixed a bug during the check for factors
       
       Obviously the number 1 and the prime number candidate itself are always factors
       and must not be checked for, since otherwise any number will be considered as
       prime.
   
   commit 6d52d36244b90f41355ee616d4f52435cc383771
   Author: Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
   Date:   Wed Sep 23 14:24:29 2020 +0200
   
       imported prime-number-test.c++ program
       
       This program checks whether a given number is prime and prints the result to
       standard output.
   ```
2. Printing the same in a format with just one line per commit:
   ```shell
   $ git log --oneline
   ```
   ```
   83b93af (HEAD -> master) imported licence information
   68ff634 improved the check for factors
   3b34a1f fixed a bug during the check for factors
   6d52d36 imported prime-number-test.c++ program
   ```
3. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   which happens per default in topological order (see option `--topo-order`), but may be done in chronological order (see option `--date-order`) as well:
   ```
   * 83b93af (HEAD -> master) imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   *Since there are no branches, yet, there is not much more to see.*

`git log` is far more powerful than what was shown above. For example, it allows to specify which files and/or which revision ranges to consider, how commits are ordered and how output is to be formatted. It can even trace the evolution of line ranges or function definitions through the history (see option `-L`). A detailed description can be found in its [documentation](https://git-scm.com/docs/git-log).




## Collaborative Git Workflow
So far, a look was taken on some typical workflows for a single developer. The following examples demonstrate a collaborative (that is: multiple developers) workflow in git.

Assumed are two developers:
* A: the original creator and probably maintainer of the project from above
* B: some other contributor


### Developer A: Preparations
In order to collaborate with other contributors the repository needs to be made available to them by some means.

While it is possible to directly use the local repository for that, this is rarely done because it’s usually not so easily accessible (behind firewall) or available all the time (computer powered off).<br/>
Instead a repository on some server (which can be some plain git server or some feature-rich git web-service) is used.

The following examples use the [GitLab web-service from the LMU Physics Faculty](https://gitlab.physik.lmu.de/), but things should work similar with other systems.

1. Unless already present, generate a personal SSH key pair:
   ```shell
   $ ssh-keygen -t ed25519
   ```
   using the default file location and some passphrase.
2. Display the SSH public key:
   ```shell
   $ cat ~/.ssh/id_ed25519.pub
   ```
   ```
   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN8LHWzhz3P/6jT+rzFeykj4ZnL3buamdSPkfh/t0jMn christoph.anton.mitterer@lmu.de
   ```
3. Log in to the git web-service.
4. Add the SSH public key to the [git web-service’s SSH key settings](https://gitlab.physik.lmu.de/-/profile/keys).
5. Create a [new project](https://gitlab.physik.lmu.de/projects/new) via the accordingly named button:<br/>
   Choose some project name, for example `git-course-example`, add a description (optional) and set the visibility to public. Do **not** choose to initialise the repository but leave it empty.
6. Determine the URL of the remote repository:<br/>
   In the web-interface there is a button “Clone” (GitHub name is “Code”), which shows the URLs for different protocols.<br/>
   SSH shall be used, for which it will be something like `git@gitlab.physik.lmu.de:mitterer/git-course-example.git` or its equivalent form `ssh://git@gitlab.physik.lmu.de/mitterer/git-course-example.git`.
7. Add a remote named `origin` with the URL from above to the local repository using [`git remote`](https://git-scm.com/docs/git-remote):
   ```shell
   $ git remote add origin ssh://git@gitlab.physik.lmu.de/mitterer/git-course-example.git
   ```

### Developer A: Pushing Changes To A Remote Repository
**Pushing** is the process of sending commits from some local branch to some remote repository’s branch.

It is performed with the command [`git push`](https://git-scm.com/docs/git-push), which can be quite complex but is simple for most normal use cases.

1. Push the `master` branch of the local repository to the empty remote repository:
   ```shell
   $ git push --set-upstream origin master
   ```
   ```
   Enumerating objects: 12, done.
   Counting objects: 100% (12/12), done.
   Delta compression using up to 4 threads
   Compressing objects: 100% (9/9), done.
   Writing objects: 100% (12/12), 1.88 KiB | 961.00 KiB/s, done.
   Total 12 (delta 2), reused 0 (delta 0), pack-reused 0
   To ssh://gitlab.physik.lmu.de/mitterer/git-course-example.git
    * [new branch]      master -> master
   Branch 'master' set up to track remote branch 'master' from 'origin'.
   ```
   The `--set-upstream` option and the explicit naming of the local branch `master` (which is here actually a part of `git push`’s `<refspec>` argument) is only necessary when first creating a new remote branch (here: the remote `master`).<br/>
   It’s not necessary on subsequent pushes, where `git push origin` will be enough.
2. Have a look at the git web-service:<br/>
   This should now show for example the files from the repository and a “4 [Commits](https://gitlab.physik.lmu.de/mitterer/git-course-example/-/commits/master)” link somewhere, which – when opened – shows the log.

It shall be noted, that pushing only succeeds if the tip of the remote branch is an ancestor of what’s being pushed (or in other words: doesn’t contain anything which is not also contained in what’s being pushed).


### Different Flows For Exchange Between Repositories
Exchanging commits between repositories (usually all clones of the same one) is done via **pushing** and **pulling** (the latter of which is actually *fetching* followed by *merging*).

Amongst others, there is a number of typical flows for this:
* Directly pushing/pulling to/from the respective other developer’s local repository, initiated by either side (depending on permissions).<br/>
  *Has the same disadvantages of accessibility and availability as outlaid above.*
* The maintainer (here: developer A) of the “central” repository on some server grants the contributor (here: developer B) some write access to it.<br/>
  These *push rights* could be limited to some branches, then typically excluding `master`.
* The contributor has his own remote repository one some server (to which he has full write access) and pushes his commits there, usually into a separate branch (in especially not `master`).
  * Variant Ⅰ<br/>
    From there the maintainer can pull them into his local repository, resolving any conflicts, and from there he can push them into the “central” one.<br/>
    Eventually the contributor can update his local `master` by pulling from the “central” one and his remote `master` by pushing into it.
  
  *alternatively and with git web-services:*
  
  * Variant Ⅱ<br/>
    The contributor creates (within the web-interface) a *pull request* for his separate branch (typically with destination `master`), which the maintainer may accept, upon which it would get merged into the “central” repository.<br/>
    In this case it’s usually the contributor’s responsibility to make sure that the pull request actually applies to the “central” `master` without any conflicts. For that he will update his local `master` by pulling (from the “central” one) and then either merge it into his separate branch (*not vice-versa*) *or* **rebase** that on the updated `master`, resolving any conflicts in both cases.<br/>
    Once the pull request got accepted, the developers can update their respective `master` branches by pulling respectively pushing (except for the “central” one, which is already up to date).

  Eventually the contributor would delete his separate branch locally and remotely.

*In the following the latter shall be shown in both variants.*


### Developer B: Preparations
Developer B has learnt about the project and wants to contribute.

1. Create a clone of the [“central” repository (from developer A)](https://gitlab.physik.lmu.de/mitterer/git-course-example) on the git web-service, which is usually called *[fork](https://gitlab.physik.lmu.de/mitterer/git-course-example/-/forks/new)* by these and done via the accordingly named button.
2. Just like above, determine the remote URL **of the forked repository** (developer B’s remote repository), which will be something like `git@gitlab.physik.lmu.de:GDuckeck/git-course-example.git` or its equivalent form `ssh://git@gitlab.physik.lmu.de/GDuckeck/git-course-example.git`
3. Clone the forked repository to some local location:
   ```shell
   $ git clone ssh://git@gitlab.physik.lmu.de/GDuckeck/git-course-example.git
   ```
   ```
   Cloning into 'git-course-example'...
   remote: Enumerating objects: 12, done.
   remote: Counting objects: 100% (12/12), done.
   remote: Compressing objects: 100% (9/9), done.
   remote: Total 12 (delta 2), reused 0 (delta 0), pack-reused 0
   Receiving objects: 100% (12/12), done.
   Resolving deltas: 100% (2/2), done.
   ```
   *There is no need to set the remote – `git clone` automatically creates one named `origin`.*


### Developer B: Branching
In order to be able to keep his `master` branches identical with `upstream`’s (which may get further commits in the meantime), developer B creates a separate branch for his commits.

1. Create a new branch with [`git branch`](https://git-scm.com/docs/git-branch):
   ```shell
   $ git branch read-number-from-stdin
   ```
2. List all (local) branches:
   ```shell
   $ git branch
   ```
   ```
   * master
     read-number-from-stdin
   ```
   `*` marks the current branch.
3. Switch to the branch using [`git checkout`](https://git-scm.com/docs/git-checkout):
   ```shell
   $ git checkout read-number-from-stdin
   ```
   ```
   Switched to branch 'read-number-from-stdin'
   ```
4. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   ```
   * 83b93af (HEAD -> read-number-from-stdin, origin/master, master) imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   tells that three branches (namely `master`, `origin/master` and `read-number-from-stdin`) are at commit `83b93af`.


### Developer B: Some Commits
Developer B thinks it would be better having the number to be checked for primality not hard-coded into the source code, but read from standard input, and also wants to add a [CMake](https://cmake.org/) configuration.

1. Modify `prime-number-test.c++` according to the following patch in unified diff format:
   ```diff
   diff --git a/prime-number-test.c++ b/prime-number-test.c++
   index 1ce986c..7bbe62e 100644
   --- a/prime-number-test.c++
   +++ b/prime-number-test.c++
   @@ -3,7 +3,11 @@
    
    int main(int argc, char* argv[])
    {
   -	int number = 73; //why 73?
   +	int number;
   +	
   +	//read number from standard input
   +	std::cout << "Enter an integer number: ";
   +	std::cin >> number;
    	
    	
    	//check whether number is a prime
   ```
2. Staging and committing the changes:
   ```shell
   $ git commit --all --message='read number to be checked for primality from stdin'
   ```
3. Create the file `CMakeLists.txt` with the following content:
   ```cmake
   project(prime-number-test)
   add_executable(prime-number-test prime-number-test.c++)
   ```
4. Staging and committing the changes:
   ```shell
   $ git add CMakeLists.txt
   $ git commit --message='imported CMake configuration'
   ```
5. Pushing the new separate branch to the remote repository (developer B’s):
   ```shell
   $ git push origin read-number-from-stdin
   ```
   ```
   Enumerating objects: 8, done.
   Counting objects: 100% (8/8), done.
   Delta compression using up to 4 threads
   Compressing objects: 100% (6/6), done.
   Writing objects: 100% (6/6), 846 bytes | 846.00 KiB/s, done.
   Total 6 (delta 1), reused 0 (delta 0), pack-reused 0
   remote: 
   remote: To create a merge request for read-number-from-stdin, visit:
   remote:   https://gitlab.physik.lmu.de/GDuckeck/git-course-example/-/merge_requests/new?merge_request%5Bsource_branch%5D=read-number-from-stdin
   remote: 
   To ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example.git
    * [new branch]      read-number-from-stdin -> read-number-from-stdin
   ```
6. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   ```
   * f373f4f (HEAD -> read-number-from-stdin, origin/read-number-from-stdin) imported CMake configuration
   * 4a8cee2 read number to be checked for primality from stdin
   * 83b93af (origin/master, master) imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   shows that the branch `read-number-from-stdin` is now *ahead* of `master` (and `origin/master`).


### Developer A: Some Meanwhile Development
Unaware of developer B’s work, developer A decides to move the primality test into a separate function:

1. Modify `prime-number-test.c++` to:
   ```c++
   #include <iostream>
   
   
   bool is_prime(int number)
   {
   	//check whether number is a prime
   	for(int i = 2; i < number; ++i)
   	{
   		if(number % i  ==  0)
   		{
   			//found a divisor, thus number cannot be prime
   			return false;
   		}
   	}
   	
   	//found no divisor, thus number must be prime
   	return true;
   }
   
   
   int main(int argc, char* argv[])
   {
   	int number = 73; //why 73?
   	
   	
   	//print results
   	if( is_prime(number) )
   	{
   		std::cout << number << " is a prime number." << std::endl;
   	}
   	else
   	{
   		std::cout << number << " is not a prime number." << std::endl;
   	}
   }
   ```
2. Staging and committing the changes:
   ```shell
   $ git add prime-number-test.c++
   $ git commit -m 'move primality test into separate function'
   ```


### Variant Ⅰ
Developer B tells developer A that he has made some improvements to be found in the branch `read-number-from-stdin` of the repository at `ssh://git@gitlab.physik.lmu.de/GDuckeck/git-course-example.git`.<br/>
Developer A is interested and wants to integrate them by pulling.

This is done using [`git pull`](https://git-scm.com/docs/git-pull), which is more or less the same than [`git fetch`](https://git-scm.com/docs/git-fetch) followed by [`git merge`](https://git-scm.com/docs/git-merge).

#### Developer A: Pulling Developer B’s Changes Form His Remote Repository
1. Add a remote named `developer-b` with the URL from above to the local repository (developer A’s):
   ```shell
   $ git remote add developer-b ssh://git@gitlab.physik.lmu.de/GDuckeck/git-course-example.git
   ```
2. Make sure the current branch is `master`, since changes shall be merged into that:
   ```shell
   $ git checkout master
   ```
3. Pull the commits from developer B’s branch `read-number-from-stdin` into the current branch (which is the local `master`).
   ```shell
   $ git pull developer-b read-number-from-stdin
   ```
   ```
   warning: Pulling without specifying how to reconcile divergent branches is
   discouraged. You can squelch this message by running one of the following
   commands sometime before your next pull:
   
     git config pull.rebase false  # merge (the default strategy)
     git config pull.rebase true   # rebase
     git config pull.ff only       # fast-forward only
   
   You can replace "git config" with "git config --global" to set a default
   preference for all repositories. You can also pass --rebase, --no-rebase,
   or --ff-only on the command line to override the configured default per
   invocation.
   
   remote: Enumerating objects: 8, done.
   remote: Counting objects: 100% (8/8), done.
   remote: Compressing objects: 100% (6/6), done.
   remote: Total 6 (delta 1), reused 0 (delta 0), pack-reused 0
   Unpacking objects: 100% (6/6), 826 bytes | 91.00 KiB/s, done.
   From ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example
    * branch            read-number-from-stdin -> FETCH_HEAD
    * [new branch]      read-number-from-stdin -> developer-b/read-number-from-stdin
   Auto-merging prime-number-test.c++
   CONFLICT (content): Merge conflict in prime-number-test.c++
   Automatic merge failed; fix conflicts and then commit the result.
   ```
   Git complains that an automatic merge wasn’t possible and asks the user to resolve the conflict.

#### Developer A: Inspecting The Failed Pull – Why Is There A Merge Conflict?
1. The repository status now shows:
   ```shell
   $ git status
   ```
   ```
   On branch master
   Your branch is ahead of 'origin/master' by 1 commit.
     (use "git push" to publish your local commits)
   
   You have unmerged paths.
     (fix conflicts and run "git commit")
     (use "git merge --abort" to abort the merge)
   
   Changes to be committed:
   	new file:   CMakeLists.txt
   
   Unmerged paths:
     (use "git add <file>..." to mark resolution)
   	both modified:   prime-number-test.c++
   
   ```
   telling, that developer A’s local repository is on branch `master` (which is 1 commit ahead of its remote repository’s counterpart, simply because he hadn’t pushed his own changes from above, yet), that a new file `CMakeLists.txt` is staged to be committed and that there is an unmerged file `prime-number-test.c++` for which both “sides” (developer A’s and developer B’s) have been modified.

So apparently the merge has succeeded for `CMakeLists.txt` but hasn’t for `prime-number-test.c++`.

2. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   ```
   * f373f4f (developer-b/read-number-from-stdin) imported CMake configuration
   * 4a8cee2 read number to be checked for primality from stdin
   | * 4b63180 (HEAD -> master) move primality test into separate function
   |/  
   * 83b93af (origin/master) imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   shows now two branches in developer A’s local repository, namely his own `master` and developer B’s `read-number-from-stdin`.

The pull above was ought to merge `developer-b/read-number-from-stdin` into `master`, so developer B’s commits `4a8cee2` and `f373f4f` must be “brought together” with developer A’s commit `4b63180`.

Since `master`’s most recent commit (`4b63180`) is not an ancestor of `developer-b/read-number-from-stdin`’s “first” commit (`4a8cee2`), developer B’s branch can’t be just simply put on top of `master` (which would be called a *fast-forward* merge).<br/>
And because developer A’s commit `4b63180` and developer B’s commit `4a8cee2` **both** touch the same file `prime-number-test.c++` in the same “area”, git cannot automatically merge them – how could it know whether it should take developer A’s version, developer B’s version or even something completely different (like a combination of both, which of course it wouldn’t be able to create).<br/>
This is where the merge conflict arises.

On the other hand, only developer B’s commit `f373f4f` touches the file `CMakeLists.txt` (it doesn’t even exist in developer A’s branch), which is why it can be automatically merged.

#### Developer A: Resolving The Merge Conflict
In variant Ⅰ it’s developer A who resolves the conflict – of course he could also just demand from developer B to update his branch so that it applies gracefully.

There are two ways of resolving the conflict, *either:*
1. Manual conflict resolution:<br/>
   *Conflict markers* have been added to `prime-number-test.c++`, hinting the places of conflict:
   ```c++
   #include <iostream>
   
   
   bool is_prime(int number)
   {
   <<<<<<< HEAD
   =======
   	int number;
   	
   	//read number from standard input
   	std::cout << "Enter an integer number: ";
   	std::cin >> number;
   	
   	
   >>>>>>> f373f4f46c188f47c774941d7cca98d6583ea3c0
   	//check whether number is a prime
   	for(int i = 2; i < number; ++i)
   	{
   		if(number % i  ==  0)
   		{
   			//found a divisor, thus number cannot be prime
   			return false;
   		}
   	}
   	
   	//found no divisor, thus number must be prime
   	return true;
   }
   
   
   int main(int argc, char* argv[])
   {
   	int number = 73; //why 73?
   	
   	
   	//print results
   	if( is_prime(number) )
   	{
   		std::cout << number << " is a prime number." << std::endl;
   	}
   	else
   	{
   		std::cout << number << " is not a prime number." << std::endl;
   	}
   }
   ```
   
   Once the conflict is resolved, the file must staged (with `git add`).

*or alternatively:*

2. Assisted conflict resolution using [`git mergetool`](https://git-scm.com/docs/git-mergetool):<br/>
   ```shell
   $ git mergetool
   ```
   ```
   Merging:
   prime-number-test.c++
   
   Normal merge conflict for 'prime-number-test.c++':
     {local}: modified file
     {remote}: modified file
   ```
   This open a *3-way-diff* in some diff-tool (here the graphical Meld):
   ![3-way-diff for a merge conflict using Meld](https://gitlab.physik.lmu.de/mitterer/git-course/-/raw/master/res/git-mergetool-variant-1.png)
   with the:
   * left side (“`LOCAL`”) being the version of the branch merged **into** (here: `master`)
   * right side (“`REMOTE`”) being the version of the branch merged **from** (here: `developer-b/read-number-from-stdin`)
   * middle being the final version, in which the conflict shall be resolved
   
   Once the conflict is resolved, especially the middle files must be saved. When the diff-tool is exited, the conflict is considered to be resolved and the file will be automatically staged.

A reasonable resolution could look like this:
```c++
#include <iostream>


bool is_prime(int number)
{
	//check whether number is a prime
	for(int i = 2; i < number; ++i)
	{
		if(number % i  ==  0)
		{
			//found a divisor, thus number cannot be prime
			return false;
		}
	}
	
	//found no divisor, thus number must be prime
	return true;
}


int main(int argc, char* argv[])
{
	int number;
	
	//read number from standard input
	std::cout << "Enter an integer number: ";
	std::cin >> number;
	
	
	//print results
	if( is_prime(number) )
	{
		std::cout << number << " is a prime number." << std::endl;
	}
	else
	{
		std::cout << number << " is not a prime number." << std::endl;
	}
}
```
which takes the “best” of both sides.

3. Deleting leftover files from the conflict resolution:
   ```shell
   $ rm -f prime-number-test.c++.orig
   ```
4. Committing the staged files, thereby finalising the (now conflict-free) merge:
   ```shell
   $ git commit
   ```
   with a commit message like the default:
   ```
   Merge branch 'read-number-from-stdin' of ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example into master
   ```

#### Developer A: Pushing And Final Inspection
1. Pushing the local `master` to the “central” repository:
   ```shell
   $ git push origin
   ```
   ```
   Enumerating objects: 14, done.
   Counting objects: 100% (14/14), done.
   Delta compression using up to 4 threads
   Compressing objects: 100% (12/12), done.
   Writing objects: 100% (12/12), 1.66 KiB | 212.00 KiB/s, done.
   Total 12 (delta 4), reused 0 (delta 0), pack-reused 0
   To ssh://gitlab.physik.lmu.de/mitterer/git-course-example.git
      83b93af..83a1657  master -> master
   ```
   from where others (including developer B) can then update theirs.
2. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   ```
   *   83a1657 (HEAD -> master, origin/master) Merge branch 'read-number-from-stdin' of ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example into master
   |\  
   | * f373f4f (developer-b/read-number-from-stdin) imported CMake configuration
   | * 4a8cee2 read number to be checked for primality from stdin
   * | 4b63180 move primality test into separate function
   |/  
   * 83b93af imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   shows how the two branches were “unified” by the merge.

#### Developer B: Updating And Cleanups
At some point developer B realises that his changes have been merged upstream.

1. Add a remote named `upstream` with the “central” repository’s URL to the local repository (developer B’s):
   ```shell
   $ git remote add upstream ssh://git@gitlab.physik.lmu.de/mitterer/git-course-example.git
   ```
2. Make sure the current branch is `master`, since that shall be updated:
   ```shell
   $ git checkout master
   ```
3. Pull the changes from `upstream`’s branch `master` into the current branch (which is the local `master`).
   ```shell
   $ git pull upstream master
   ```
   ```
   warning: Pulling without specifying how to reconcile divergent branches is
   discouraged. You can squelch this message by running one of the following
   commands sometime before your next pull:
   
     git config pull.rebase false  # merge (the default strategy)
     git config pull.rebase true   # rebase
     git config pull.ff only       # fast-forward only
   
   You can replace "git config" with "git config --global" to set a default
   preference for all repositories. You can also pass --rebase, --no-rebase,
   or --ff-only on the command line to override the configured default per
   invocation.
   
   remote: Enumerating objects: 10, done.
   remote: Counting objects: 100% (10/10), done.
   remote: Compressing objects: 100% (6/6), done.
   remote: Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
   Unpacking objects: 100% (6/6), 823 bytes | 274.00 KiB/s, done.
   From ssh://gitlab.physik.lmu.de/mitterer/git-course-example
    * branch            master     -> FETCH_HEAD
    * [new branch]      master     -> upstream/master
   Updating 83b93af..83a1657
   Fast-forward
    CMakeLists.txt        |  2 ++
    prime-number-test.c++ | 26 +++++++++++++++++---------
    2 files changed, 19 insertions(+), 9 deletions(-)
    create mode 100644 CMakeLists.txt
   ```
4. Pushing the (now up to date) local `master` to the remote repository (developer B’s):
   ```shell
   $ git push origin
   ```
   ```
   Enumerating objects: 10, done.
   Counting objects: 100% (10/10), done.
   Delta compression using up to 4 threads
   Compressing objects: 100% (6/6), done.
   Writing objects: 100% (6/6), 843 bytes | 843.00 KiB/s, done.
   Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
   To ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example.git
      83b93af..83a1657  master -> master
   ```
5. Deleting the now obsolete branch `read-number-from-stdin`:<br/>
   …from the local repository, via `git branch`’s options `--delete` respectively `-d`:
   ```shell
   $ git branch --delete read-number-from-stdin
   ```
   ```
   Deleted branch read-number-from-stdin (was f373f4f).
   ```
   and from the remote repository (developer B’s), via `git push`’s options `--delete` respectively `-d`:
   ```shell
   $ git push --delete origin read-number-from-stdin
   ```
   ```
   To ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example.git
    - [deleted]         read-number-from-stdin
   ```

#### Developer B: Final Inspection
1. Printing a text-based graphical representation of the commit tree of all branches:
   ```shell
   $ git log --all --oneline --graph
   ```
   ```
   *   83a1657 (HEAD -> master, upstream/master, origin/master) Merge branch 'read-number-from-stdin' of ssh://gitlab.physik.lmu.de/GDuckeck/git-course-example into master
   |\  
   | * f373f4f imported CMake configuration
   | * 4a8cee2 read number to be checked for primality from stdin
   * | 4b63180 move primality test into separate function
   |/  
   * 83b93af imported licence information
   * 68ff634 improved the check for factors
   * 3b34a1f fixed a bug during the check for factors
   * 6d52d36 imported prime-number-test.c++ program
   ```
   shows, that apart from some different remote names and the already removed branch `read-number-from-stdin`, this is same than developer A’s repository (as inspected above).




## Copyright And Licences
Copyright © 2022, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.<br/>
All rights reserved.


This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by/4.0/).<br/>
![CC-BY-SA Logo](https://gitlab.physik.lmu.de/mitterer/git-course/-/raw/master/res/cc-by-sa.svg)

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.<br/>
![GFDL Logo](https://gitlab.physik.lmu.de/mitterer/git-course/-/raw/master/res/gfdl.svg)
